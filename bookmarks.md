# les principaux liens lus/utilisés

## Sur GIT
* [Video GIT/GitHUB sur la chaîne EUDUREKA!](https://www.youtube.com/watch?v=xuB1Id2Wxak) : 1h+ mais très didactique
* [Most Basic Git Commands with Examples – a Welcome Introduction to Git](https://rubygarage.org/blog/most-basic-git-commands-with-examples)
* [Guide for beginners @stackoverflow.com](https://stackoverflow.com/questions/315911/git-for-beginners-the-definitive-practical-guide)

## Sur GitLab
* [Installation](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [How to create your SSH Keys](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)
* [GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html)

