# Introduction
Projet utilisé pour tester GIT et GitLab.  
Histoire de mettre en oeuvre les différents tutoriaux lus sur le sujet.

Ce README sert de TOC pour les différents chapitre/fichiers du projet.

# TOC
* git_cheatsheet.md : les commandes de bases de GIT
* bookmarks.md : les principaux liens lus/utilisés.


