# Commandes de base (cheat sheet)

## Configuring Git

```
# Git will use the same username and email to mark your commits
$ git config --global user.name "King Kong"
$ git config --global user.email "king-kong@gmail.com"
 
# have some pretty colors for the output
$ git config --global color.ui true
 
#view your Git configurations
$ git config --list
```

## Initialisation d'un projet

```
# Starting a Local Repository based on a GilLab repo
cd ~/develop
git clone git@gitlab.com:pierre.calizzano/Learning-Git.git
 
# Starting a New Local Repository
cd ~ /develop/myproject/
$ git init
 
#to know the status of your repo
$ git status
```

## Actions sur les sources d'un projet

```
git add <file name>
git commit -m "<Description>"
```

## Synchro entre repo local et distant

```
#report des commit du Local vers le serveur Distant
git push -u origin master
 
# initialisation à partir d'un répertoire existant
cd existing_folder
git init
git remote add origin git@gitlab.com:pierre.calizzano/Learning-Git.git
git add .
git commit -m "Initial commit"
git push -u origin master
 
# Créer un repo Central à partir d'un repo Local
cd existing_repo
git remote add origin git@gitlab.com:pierre.calizzano/Learning-Git.git
git push -u origin --all
git push -u origin --tags
```

